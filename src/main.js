import Vue from 'vue';
import App from './App.vue';
import VueRouter from 'vue-router';
import { BootstrapVue, BootstrapVueIcons } from 'bootstrap-vue';
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap-vue/dist/bootstrap-vue.css';
import VueLazyload from 'vue-lazyload';
import Vuelidate from "vuelidate/src";
import '@fortawesome/fontawesome-free/css/all.css'
import '@fortawesome/fontawesome-free/js/all.js'
// Configurations
import { store } from './store/store';
import { router } from './router/router';
import axios from 'axios';

// Constants
import Constants from './conf/constants'

Vue.config.productionTip = false;

Vue.use(VueRouter);
Vue.use(BootstrapVue);
Vue.use(BootstrapVueIcons);
Vue.use(store);
Vue.use(VueLazyload);
Vue.use(Vuelidate);
Vue.use(Constants);


axios.defaults.baseURL = "https://negoziacasatua-api-zb2j4lvwaq-ew.a.run.app/api/v2";
// axios.defaults.baseURL = "http://localhost:5000/api/v2";  // for local develop

new Vue({
  router: router,
  store: store,
  render: h => h(App),
}).$mount('#app');
