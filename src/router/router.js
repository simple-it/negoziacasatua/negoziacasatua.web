import Vue from 'vue';
import VueRouter from 'vue-router'
import { store } from '../store/store'

import Home from "../views/Home/Home";
import Login from "../views/Login/Login";
import CategoryDetails from "../views/CategoryDetails/CategoryDetails";
import Dashboard from "../views/Dashboard/Dashboard";
import SearchResults from "../views/SearchResults/SearchResults";

export const router = new VueRouter({
    mode: 'history',
    base: process.env.BASE_URL,
    routes: [
        {
            path: '/',
            name: 'Home',
            component: Home
        },
        {
            path: '/about',
            name: 'Il Progetto',
            component: () => import(/*webpackChunkName: "Il progetto"*/"../views/About/About"),
        },
        {
            path: '/login',
            name: 'Login',
            component: Login,
            beforeEnter(to, from, next) {
                if (store.getters.isAuthenticated) {
                    next('/dashboard')
                } else {
                    next()
                }
            },
        },
        {
            path: '/registrazione',
            name: 'ShopRegistration',
            component: () => import(/*webpackChunkName: "Registra negozio"*/"../views/Registration/Children/ShopRegistration/ShopRegistration"), // ci si può registrare solo come negozi
        },
        {
            path: '/cerca/:toSearch/:nResults/:nPage',
            name: 'SearchStores',
            component: SearchResults
        },
        {
            path: '/categoria/:categoryid',
            name: 'CategoryDetails',
            component: CategoryDetails,
        },
        {
            path: '/negozio/:shopid',
            name: 'ShopDetails',
            component: () => import(/*webpackChunkName: "Dettagli negozio"*/"../views/ShopDetails/ShopDetails")
        },
        {
            path: '/dashboard',
            name: 'Dashboard',
            component: Dashboard,
            beforeEnter(to, from, next) {
                store.getters.isAuthenticated ? next() : next(false)
            },
            children: [
                {
                    path: 'lista-negozi',
                    name: 'ShopList',
                    component: () => import(/*webpackChunkName: "Lista negozi"*/"../views/Dashboard/Children/ShopList/ShopList"),
                    beforeEnter(to, from, next) {
                        store.getters.accountTypeId === Vue.Account.ADMIN
                            || store.getters.accountTypeId === Vue.Account.MUNICIPAL ? next() : next(false)
                    }
                },
                {
                    path: 'validazione-negozi',
                    name: 'ShopValidation',
                    component: () => import(/*webpackChunkName: "Validazione negozi"*/"../views/Dashboard/Children/ShopValidation/ShopValidation")
                },
                {
                    path: 'lista-account',
                    name: 'AccountList',
                    component: () => import(/*webpackChunkName: "Lista account"*/"../views/Dashboard/Children/AccountList/AccountList")
                },
                {
                    path: 'crea-negozio',
                    name: 'ShopCreation',
                    component: () => import(/*webpackChunkName: "Crea negozio"*/"../views/Dashboard/Children/ShopCreation/ShopCreation") ,
                    beforeEnter(to, from, next) {
                        store.getters.accountTypeId === Vue.Account.ADMIN
                            || store.getters.accountTypeId === Vue.Account.MUNICIPAL? next() : next(false)
                    }
                },
                {
                    path: 'modifica-negozio/:shopId',
                    name: 'ShopEdit',
                    component: () => import(/*webpackChunkName: "Modifica negozio"*/"../views/Dashboard/Children/ShopEdit/ShopEdit"),
                }
            ]
        },
        { path: '*', redirect: '/' },
    ],
    scrollBehavior (to, from, savedPosition) {
        if (savedPosition) {
            return savedPosition;
        } else {
            return {x: 0, y: 0}
        }
    }
});
