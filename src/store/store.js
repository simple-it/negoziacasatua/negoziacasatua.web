import Vue from 'vue';
import Vuex from 'vuex';
import createPersistedState from "vuex-persistedstate";

Vue.use(Vuex);

// IMPORT MODULES
import user from "../store/modules/user";

export const store = new Vuex.Store({
    plugins: [
        createPersistedState({
            paths:['user']
        })
    ],
    state: {},
    mutations: {},
    actions: {},
    getters: {},
    modules: {
        user: user
    }
});


