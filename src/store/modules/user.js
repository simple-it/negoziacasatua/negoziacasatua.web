import Axios from "axios";
import {router} from "@/router/router";

const state = {
    id: null,
    email: null,
    storeName: null,
    storeId: null,
    accountTypeId: null,
    token: null,
    tokenExpiration: null,
    error: null,
};

const getters = {
    isAuthenticated: state => {
        return state.token !== null && state.token !== '';
    },
    accountTypeId: state => {
        return state.accountTypeId;
    },
    errorOccurred: state => {
        return state.error !== null;
    },
    error: state => {
        return state.error;
    },
    token: state => {
        return state.token;
    },
    username: state => {
        return state.email;
    },
    storeId: state => {
        return state.storeId;
    }
};

const mutations = {
    authenticateUser(state, userData) {
        state.id = userData.id;
        state.email = userData.email;
        state.storeName = userData.storeName;
        state.storeId = userData.storeId;
        state.accountTypeId = userData.accountTypeId;
        state.token = userData.token;
        state.tokenExpiration = userData.tokenExpiration;

        router.push({name: 'Dashboard'});

    },
    deauthenticateUser(state) {
        state.id = null;
        state.email = null;
        state.storeName = null;
        state.storeId = null;
        state.accountTypeId = null;
        state.token = null;
        state.tokenExpiration = null;
        state.error = null;

        router.push({name: 'Home'})

    },
    setError(state, error) {
        state.error = error;
    },
    clearError(state) {
        state.error = null;
    }
};

const actions = {
    login({commit}, authData) {
        Axios({
            method: "POST",
            "url": "/account/login",
            data: {
                "email": authData.email,
                "password": authData.password
            }
        }).then((response) => {
            commit('authenticateUser', response.data);
        }).catch((error) => {
            commit('setError', error.response.data.error);
        });
    }
};

export default {
    state: state,
    getters: getters,
    mutations: mutations,
    actions: actions
}
